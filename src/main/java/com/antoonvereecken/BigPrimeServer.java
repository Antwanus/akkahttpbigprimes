package com.antoonvereecken;

import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.AskPattern;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpEntities;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.Route;
import akka.stream.FlowShape;
import akka.stream.Graph;
import akka.stream.SinkShape;
import akka.stream.UniformFanOutShape;
import akka.stream.javadsl.*;
import akka.stream.typed.javadsl.ActorFlow;
import akka.util.ByteString;
import com.vpp.PrimeDatabaseActor;

import java.math.BigInteger;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static akka.http.javadsl.server.Directives.*;
import static akka.http.javadsl.server.PathMatchers.*;

public class BigPrimeServer {
    ActorSystem<PrimeDatabaseActor.Command> actorSys = ActorSystem.create(PrimeDatabaseActor.create(), "actor_S");

    private CompletableFuture<HttpResponse> newPrimeNumberRequest() {
        CompletionStage<Integer> requestId = AskPattern.ask(
                actorSys,                   PrimeDatabaseActor.NewRequestCommand::new,
                Duration.ofSeconds(5),      actorSys.scheduler()
        );
        CompletableFuture<HttpResponse> response = new CompletableFuture<>();
        requestId.whenComplete((id, throwable) -> response.complete(HttpResponse.create()
                                                                        .withStatus(200)
                                                                        .withEntity(id.toString())));
        return response;
    }
    /** NEW_UPDATE_REQUEST CURRENTLY HAS 3 VERSIONS */
    private CompletableFuture<HttpResponse> newUpdateRequest(String requestId) {
        CompletionStage<BigInteger> resultValue = AskPattern.ask(actorSys,
                me -> new PrimeDatabaseActor
                        .GetResultCommand(Integer.parseInt(requestId), me),
                Duration.ofSeconds(5),
                actorSys.scheduler());
        CompletableFuture<HttpResponse> response = new CompletableFuture<>();
        resultValue.whenComplete( (value, throwable) -> response.complete(HttpResponse.create()
                                                                                .withStatus(200)
                                                                                .withEntity(value.toString())));
        return response;
    }

    Flow<Integer, Integer, NotUsed> loggingFLOW = Flow.of(Integer.class).map (id -> {
        System.out.println("Received progress update request for Id " + id);
        return id;
    });
    Flow<Integer, BigInteger, NotUsed> getProgressFLOW =
            ActorFlow.ask(actorSys, Duration.ofSeconds(5),
                    PrimeDatabaseActor.GetResultCommand::new);
    private Route newUpdateRequestV2(String requestId) {
        Source<BigInteger, NotUsed> source = Source.single(1)
                .map (x -> Integer.parseInt(requestId))
                .via(loggingFLOW)
                .via(getProgressFLOW);

        Source<ByteString, NotUsed> getUpdateSource =
                source.map( x -> ByteString.fromString(x.toString()));

        return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, getUpdateSource));

    }
    private Route newUpdateRequestV3(Integer requestId) {
        /*     log_FLOW    ->     progress_FLOW ->     BROADCAST(2) =>
         *                     1 = byteString_FLOW     ->      { Akka uses this for HttpRequests }
         *                     2 = filterNotZero_FLOW  ->      System.out_SINK
         */
        Graph<FlowShape<Integer, ByteString>, NotUsed> partialGraph = GraphDSL.create(
            builder -> {
                FlowShape<Integer, Integer> loggingFlowShape = builder.add(loggingFLOW);
                FlowShape<Integer, BigInteger> getProgressShape = builder.add(getProgressFLOW);

                UniformFanOutShape<BigInteger, BigInteger> broadcast = builder.add( Broadcast.create(2));
                    FlowShape<BigInteger, BigInteger> filterShape = builder.add(
                            Flow.of(BigInteger.class)
                                    .filter (i -> !i.equals(BigInteger.ZERO)));
                    FlowShape<BigInteger, ByteString> byteStringShape = builder.add(
                            Flow.of(BigInteger.class)
                                    .map( i -> ByteString.fromString(i.toString())));

                    SinkShape<BigInteger> sinkShape = builder.add( Sink.foreach(System.out::println));

                builder.from(loggingFlowShape).via(getProgressShape)
                        .viaFanOut(broadcast).via(byteStringShape);
                builder.from(broadcast).via(filterShape).to(sinkShape);

                return FlowShape.of(loggingFlowShape.in(), byteStringShape.out());
        });

        Source<ByteString, NotUsed> sourceFromStream = Source.single(requestId)
                .via(partialGraph);
        return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, sourceFromStream));
    }

    public Route createRoute() {
        return get( () ->
            concat(
                    pathEndOrSingleSlash( () -> {
                                System.out.println("Received new request");
                                return completeWithFuture(newPrimeNumberRequest());
                    }),
                    path( segment("result").slash(integerSegment() ),
                        (Integer requestId) -> {
                        CompletionStage<Boolean> result = AskPattern.ask(
                                actorSys,
                                replyTo ->
                                        new PrimeDatabaseActor.VerifyRequestExistsCommand(replyTo, requestId),
                                Duration.ofSeconds(5),
                                actorSys.scheduler()
                        );
                        try {
                            if(Boolean.TRUE.equals(result.toCompletableFuture().get()))
                                return reject(); // reject route, passing it on to the next candidate-route in queue
                            else
                                return complete(StatusCodes.BAD_REQUEST);
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            return complete(StatusCodes.INTERNAL_SERVER_ERROR);
                        }
                    }),

                    path(segment("result").slash(integerSegment()), this::newUpdateRequestV3)
            ));
    }

    public void run() {
        Http.get(actorSys).newServerAt("localhost",8080).bind(createRoute());
    }
}
